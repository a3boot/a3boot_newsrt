var express = require('express');
var menu = require('./right_side');
var router = express.Router();

var lang = require('../bin/langKey.js');

/* GET home page. */
router.get('/', function( req, res, next)
{
    req.session.lang = req.session.lang || 'ua';

    sess = req.session;
    languageSess = lang.funcLang(req.session.lang, 'about_us_page');
    var men = menu.rightSide(req,res,next, languageSess);
    
    languageSess.title = 'site a3boot';
    languageSess.menu = men;


    res.render
    ( 'about_us_page',languageSess );
});
module.exports = router;