/**
 * Created by oksana on 10.08.2016.
 */
var nodemailer = require('nodemailer');
var express = require('express');
var session = require('express-session');
var sql = require('mysql');
var url=require('url');
var config = require('../config');
/**
 * mail for activation
 */
module.exports.sendMailActivation=function(req, res, next) {
    var transporter = nodemailer.createTransport(config.mailServer);
    var mailOptions = {
        from: config.mailFrom, // sender address
        to: req.body.emailReg, // list of receivers
        subject: 'Activation Registration', // Subject line
        text: 'Hello,'+req.body.firstName+' '+req.body.lastName+'. If you want to activate your account go to '+res.rLink+'. If you don\'t want to actevate your account do nothing. Thank you.', // plaintext body
        html: '<b>Hello,'+req.body.firstName+' '+req.body.lastName+'.</b><br> If you want to activate your account at a3boot.com.ua go to <a><a href=\"http://'+config.pathToMail+'/actReg?:actReg='+res.rLink+'\#openModal2\">'+res.rLink+'</a></b>.<br>If you don\'t want to actevate your account do nothing. Thank you.' // plaintext body
        // html body
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        }
        else {

            console.log('Message sent: ' + info.response);

        }

    });   
};
/**
 * mail for miss password
 */
module.exports.sendMailMissPassword=function(req, res, next) {

    var transporter = nodemailer.createTransport(config.mailServer);
    var mailOptions = {
        from: config.mailFrom, // sender address
        to: req.body.mail, // list of receivers
        subject: 'Forgot Password', // Subject line
        text: 'Hello,' + res.firstName + ' ' + res.lastName + '. If you want to recover your password go to ' + res.link + '.Code is valid for the one day. If you don\'t want to recover your password do anything. Thank you.', // plaintext body
        html: '<b>Hello,' + res.firstName + ' ' + res.lastName + '.</b><br> If you want to recover your password at a3boot.com.ua go to <a><a href=\"http://'+config.pathToMail+'/missPassword?:missPass=' + res.link + '\#openModal2\">' + res.link + '</a></b>.<br>Code is valid for one day.<br>If you don\'t want to recover your password do nothing. Thank you.' // plaintext body
        // html body
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        }
        else {

            console.log('Message sent: ' + info.response);

        }

    });
};