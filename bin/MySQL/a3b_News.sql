-- MySQL dump 10.13  Distrib 5.7.13, for Linux (i686)
--
-- Host: localhost    Database: a3b
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `News`
--

DROP TABLE IF EXISTS `News`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `News` (
  `ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `NewsDate` date DEFAULT NULL,
  `Header` varchar(255) DEFAULT NULL,
  `Keywords` varchar(255) DEFAULT NULL,
  `Description` varchar(150) DEFAULT NULL,
  `News` mediumtext,
  `id_User` mediumint(9) DEFAULT NULL,
  `id_Lang` mediumint(9) DEFAULT NULL,
  `Alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NewsDate` (`NewsDate`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `News`
--

LOCK TABLES `News` WRITE;
/*!40000 ALTER TABLE `News` DISABLE KEYS */;
INSERT INTO `News` VALUES (1,'2016-06-14','Почали освоювати MySQL','MySQL, початок','перша новина','Нічого не стоїть на місці, навіть наша команда./nСьогодні ми побачили свою базу MySQL і вже пробуємо з нею працювати. В тому числі і через загадковий Node.js',1,NULL,NULL),(2,'2016-06-17','Робота з базою','MySQL, початок','друга новина','Нічого не стоїть на місці, навіть наша команда./nСьогодні ми додали новий запис.',2,NULL,NULL),(3,'2016-06-24','Робота з сервером','MySQL, початок','третя новина','<p>Нічого не стоїть на місці, навіть наша команда.<br>Сьогодні ми додали два нових записи.<br></p>',2,NULL,NULL),(4,NULL,'Заголовок якийсь','ключове слово, ключове слово два','якийсь опис','текст статті, також якийсь там',NULL,NULL,'same aliass'),(5,NULL,'dfdgdfg','fdgdfgdfg? dfgdfg dfg d, dfgdfg d,fg dfg, df g','gfgdfgdfgfd','gdfgdfgdfgdfg',NULL,NULL,'dfgdfgdfgdfgdfg'),(6,'2016-07-08','Заголовок якийсь 1','вкпвп 56, ап6рп, парар','пвапа апр ар оп','6516515615 561 51561',NULL,NULL,'same aliass 1'),(7,'2016-08-05','dfgfggfh 55555','klgjkhjkgjk','gfdg.fhm;lgf,mlgfm',';lfdmb;lfgmh;lgfmnl;gf',NULL,NULL,'dfgfdg66666');
/*!40000 ALTER TABLE `News` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-08 13:02:49
