-- MySQL dump 10.13  Distrib 5.7.13, for Linux (i686)
--
-- Host: localhost    Database: a3b
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actReg`
--

DROP TABLE IF EXISTS `actReg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actReg` (
  `email` mediumtext,
  `link` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actReg`
--

LOCK TABLES `actReg` WRITE;
/*!40000 ALTER TABLE `actReg` DISABLE KEYS */;
INSERT INTO `actReg` VALUES ('vsafdsa@dfasfasdf','prdleizrjqsalzx'),('fdsaf@123123','nebadhdvsfypuxl'),('qwer@123fsa','mthfcxxcdipdwcm'),('f1826378@mvrht.com','dnglhgrlhutimvn'),('f1827186@mvrht.com','fcckcrsjteueaew'),('f1828247@mvrht.com','vtsxarcqpyvloqj'),('f1828914@mvrht.com','rshfvlwbvhqqegc'),('f1829587@mvrht.com','lpoyoyuovzuvzea'),('f1830276@mvrht.com','ufvgpfvryawmxpy'),('f1831778@mvrht.com','oxsbyabqpztvatu'),('izaydoom@10mail.org','ocfwqqwhqlrqbtn'),('f1832475@mvrht.com','ezbldsmrzsomoyv'),('f1832726@mvrht.com','hbqsvokegzbyevr'),('f1843747@mvrht.com','xccdqgbazuvrtxr'),('f1844459@mvrht.com','ltjickjrtdntauy'),('f1954967@mvrht.com','egsaxyweghjwfun'),('f2253911@mvrht.com','ilyqrcqrlwrbroy'),('f2264703@mvrht.com','adqerxyhmunsacq'),('f2264875@mvrht.com','gtzmuxpkaxdbzit');
/*!40000 ALTER TABLE `actReg` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-08 13:02:49
