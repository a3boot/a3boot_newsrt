-- MySQL dump 10.13  Distrib 5.7.13, for Linux (i686)
--
-- Host: localhost    Database: a3b
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `FName` varchar(100) DEFAULT NULL,
  `LName` varchar(100) DEFAULT NULL,
  `Nick` varchar(100) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Tel` varchar(18) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  `ActivationDate` date DEFAULT NULL,
  `Id_role` int(11) DEFAULT NULL,
  `Avatar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'Оксана','Захарчук','Ksenia','111111','oksana-suhovich@mail.ru','22222222',1,NULL,1,NULL),(2,'Коля','Петросян','Kolia','333333','kolia@mail.ru','3333333',NULL,NULL,NULL,NULL),(3,'Roman','Boyko','ItsMyLife','r02081996','roman1roman2roman1@gmail.com','380634119972',1,NULL,NULL,NULL),(4,'Roman','Boyko','ItsMyLife','r02081996','roman1roman2@mail.ru','12341249132',1,'2016-07-14',NULL,NULL),(5,'sdfads','adsf','ItsMyLife','r02081996','roman1roman2@mail.ru','23124',NULL,'2016-07-28',NULL,NULL),(6,'ivan','ivanov','asdfasd','r02081996','roman1roman2@mail.ru','adsfadsf',NULL,'2016-07-28',NULL,NULL),(7,'ivan','ivanov','roman','r02081996','roman@r','12341249132',NULL,'2016-07-28',NULL,NULL),(8,'ivan','ivanov','roman','r02081996','roman@r','12341249132',NULL,'2016-07-28',NULL,NULL),(9,'asdf','sdaf','ItsMyLife','r02081996','roman1roman2@mail.ru','sadfasdfasdf',NULL,'2016-07-28',NULL,NULL),(10,'qwerty','qwerty','topopop','12345','roman@rrrrr','1234556',NULL,'2016-07-28',NULL,NULL),(11,'rtyy','asdff','tyhgf','12345','rda123@12345','2345612',NULL,'2016-07-28',NULL,NULL),(12,'1234123','1324','uiop','sdf','rrrrrrrr@123421','1234151261',NULL,'2016-07-28',NULL,NULL),(13,'boyko123','romansd123','uyrtrewqq','123','roman@rffdsfsd','12345',NULL,'2016-07-28',NULL,NULL),(14,'bbb','rrr','qwe','ewq','rrr@123','1234',NULL,'2016-08-01',NULL,NULL),(15,'123NaN123','123',NULL,NULL,NULL,NULL,NULL,'2016-08-03',NULL,NULL),(16,'123NaN123','123',NULL,NULL,NULL,NULL,NULL,'2016-08-03',NULL,NULL),(17,'123NaN123','123',NULL,NULL,NULL,NULL,NULL,'2016-08-03',NULL,NULL),(18,'123NaN123','123',NULL,'123',NULL,NULL,NULL,'2016-08-03',NULL,NULL),(19,'123NaN123','123',NULL,'123','roman1roman2@mail.ruNaN123',NULL,NULL,'2016-08-03',NULL,NULL),(20,'132NaN132','132',NULL,'123','1234@fsd1NaNItsMyLife',NULL,NULL,'2016-08-03',NULL,NULL),(21,'123NaN123','123',NULL,'231','213@123NaNItsMyLife',NULL,NULL,'2016-08-03',NULL,NULL),(22,NULL,'123firstName=123NaN123emailReg=123@123NaNItsMyLifepasswordReg=123',NULL,NULL,NULL,NULL,NULL,'2016-08-03',NULL,NULL),(23,'roman3','boyko3','rrrr','123','roman@qwer','123',NULL,'2016-08-03',NULL,NULL),(24,'123','123','1234321','123','romasdn@123','123',NULL,'2016-08-03',NULL,NULL),(25,'qwet','qwert','213','123','123456@123','123444',NULL,'2016-08-03',NULL,NULL),(26,'123','123','asdfgssdf','123','dsfasdf@fasdgasdf','123',NULL,'2016-08-04',NULL,NULL),(27,'123','123','qwerttyu','123','vsafdsa@dfasfasdf','123',NULL,'2016-08-04',NULL,NULL),(28,'123','123','qwerty1','123','fdsaf@123123','123',NULL,'2016-08-04',NULL,NULL),(29,'132','12321','fsdaf','123','qwer@123fsa','123',NULL,'2016-08-04',NULL,NULL),(30,'qwe','qwertq','qwe11111','qwe','f1826378@mvrht.com','qwe',NULL,'2016-08-04',NULL,NULL),(31,'123','123','fdsav','123','f1827186@mvrht.com','123',NULL,'2016-08-04',NULL,NULL),(32,'123','123','f1828247@mvrht.com','123','f1828247@mvrht.com','1234',NULL,'2016-08-04',NULL,NULL),(33,'12312','123','fdsa','123','f1828914@mvrht.com','123',NULL,'2016-08-04',NULL,NULL),(34,'123','123','fdsaqwe','1234','f1829587@mvrht.com','123',NULL,'2016-08-04',NULL,NULL),(35,'123','123','f1830276@mvrht.com','123','f1830276@mvrht.com','123',1,'2016-08-04',NULL,NULL),(36,'123','123','f1831778@mvrht.com','123','f1831778@mvrht.com','123',1,'2016-08-04',NULL,NULL),(37,'123','123','izaydoom@10mail.org','123','izaydoom@10mail.org','123',NULL,'2016-08-04',NULL,NULL),(38,'123','123','f1832475@mvrht.com','123','f1832475@mvrht.com','123',NULL,'2016-08-04',NULL,NULL),(39,'123','123','f1832726@mvrht.com','123','f1832726@mvrht.com','123',1,'2016-08-04',NULL,NULL),(40,'123','123','f1843747@mvrht.com','132','f1843747@mvrht.com','123`',1,'2016-08-04',NULL,NULL),(41,'123','123','f1844459@mvrht.com','123','f1844459@mvrht.com','123',1,'2016-08-04',NULL,NULL),(42,'rrrrrr','yiytit','yry','121212','f1954967@mvrht.com','34433444',NULL,'2016-08-05',NULL,NULL),(43,'123','123','f2253911@mvrht.com','r02081996!','f2253911@mvrht.com','123',1,'2016-08-08',2,NULL),(44,'123','123','f2264703@mvrht.com','r02081996!','f2264703@mvrht.com','123',NULL,'2016-08-08',2,NULL),(45,'123','123','f2264875@mvrht.com','r02081996!','f2264875@mvrht.com','123',1,'2016-08-08',2,NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-08 13:02:48
