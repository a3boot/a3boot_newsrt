TO RUN SERVER LOCALLY YOU NEED:

1. install NODE.JS version >= 4.4.7 on your machine

2. go to root directory (where the 'package.json' file located)

3. run command "npm install" to install all dependencies

4. create mysql database "a3b" import tables from "bin/MySQL/" and change database configuration in "bin/config"

5. change host and pathToMail (website name) configuration in "bin/config"

6. run command "npm run start" to run server on localhost:8080 (see "bin/config to change port number ")

7. to add News use  path "/add_new_news" to website name
HAVE FUN! :)