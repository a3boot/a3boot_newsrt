function registration() {

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if(xhr.responseText) {
                document.getElementById('anotherEmail').innerHTML = xhr.responseText;
            }else{
                window.location.href="/";
            }
        }else if (xhr.readyState == 4 && xhr.status != 200) {
            console.dir('error');
        }
    };
    xhr.open('POST', '/registration', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    var regInfo='lastName='+document.getElementById('lastName').value+'&firstName='+document.getElementById('firstName').value+
        '&phoneNumber='+document.getElementById('phoneNumber').value+'&emailReg='+document.getElementById('emailReg').value+
        '&loginReg='+document.getElementById('loginReg').value+'&passwordReg='+document.getElementById('passwordReg').value;
    xhr.send(regInfo);
}