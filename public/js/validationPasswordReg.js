function checkPass ()
{
    with (document) {
        var password = getElementById('passwordReg').value; // Получаем пароль из формы
        var s_letters = "qwertyuiopasdfghjklzxcvbnm"; // Буквы в нижнем регистре
        var b_letters = "QWERTYUIOPLKJHGFDSAZXCVBNM"; // Буквы в верхнем регистре
        var digits = "0123456789"; // Цифры
        var specials = "!@#$%^&*()_-+=\|/.,:;[]{}"; // Спецсимволы
        var is_s = false; // Есть ли в пароле буквы в нижнем регистре
        var is_b = false; // Есть ли в пароле буквы в верхнем регистре
        var is_d = false; // Есть ли в пароле цифры
        var is_sp = false; // Есть ли в пароле спецсимволы
        for (var i = 0; i < password.length; i++) {

            /* Проверяем каждый символ пароля на принадлежность к тому или иному типу */
            if (!is_s && s_letters.indexOf(password[i]) != -1) is_s = true;
            else if (!is_b && b_letters.indexOf(password[i]) != -1) is_b = true;
            else if (!is_d && digits.indexOf(password[i]) != -1) is_d = true;
            else if (!is_sp && specials.indexOf(password[i]) != -1) is_sp = true;
        }
        var rating = 0;
        var text = "";
        var color;
        if (is_s) rating++; // Если в пароле есть символы в нижнем регистре, то увеличиваем рейтинг сложности
        if (is_b) rating++; // Если в пароле есть символы в верхнем регистре, то увеличиваем рейтинг сложности
        if (is_d) rating++; // Если в пароле есть цифры, то увеличиваем рейтинг сложности
        if (is_sp) rating++; // Если в пароле есть спецсимволы, то увеличиваем рейтинг сложности

        /* Далее идёт анализ длины пароля и полученного рейтинга, и на основании этого готовится текстовое описание сложности пароля */
        var firstRow = getElementById("translate").innerHTML;
        switch  (firstRow){
            case 'Заповніть усі поля:':
            {
                if (password.length < 6 && rating < 3){ text = "Занадто простий пароль"; color="red"}
                else if (password.length < 6 && rating >= 3) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 8 && rating < 3) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Хороший пароль"; color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Занадто простий пароль."; color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Хороший пароль";color="green"}
                getElementById('bad_pass1').innerHTML = text;

                // Выводим итоговую сложность пароля
                //return false;
                break
            }
            case 'Fill in all fields:':
            {
                if (password.length < 6 && rating < 3) {text = "Too simple password"; color="red"}
                else if (password.length < 6 && rating >= 3) {text = "The average password";color="yellow"}
                else if (password.length >= 8 && rating < 3) {text = "The average password";color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Good password";color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Too simple password"; color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "The average password";color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Good password";color="green"}
                getElementById('bad_pass1').innerHTML = text; // Выводим итоговую сложность пароля
                //return false;
                break
            }
            case 'Заполните все поля:':
            {
                if (password.length < 6 && rating < 3){ text = "Слишком простой пароль";color="red"}
                else if (password.length < 6 && rating >= 3) {text = "Средний пароль";color="yellow"}
                else if (password.length >= 8 && rating < 3){text = "Средний пароль";color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Хороший пароль";color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Слишком простой пароль";color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "Средний пароль";color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Хороший пароль";color="green"}
                getElementById('bad_pass1').innerHTML = text; // Выводим итоговую сложность пароля
                //return false;
                break
            }
            case 'Надіслати':
            {
                if (password.length < 6 && rating < 3){ text = "Занадто простий пароль"; color="red"}
                else if (password.length < 6 && rating >= 3) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 8 && rating < 3) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Хороший пароль"; color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Занадто простий пароль."; color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "Середний пароль"; color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Хороший пароль";color="green"}
                getElementById('bad_pass1').innerHTML = text;
                break
            }
            case 'Submit':
            {
                if (password.length < 6 && rating < 3) {text = "Too simple password"; color="red"}
                else if (password.length < 6 && rating >= 3) {text = "The average password";color="yellow"}
                else if (password.length >= 8 && rating < 3) {text = "The average password";color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Good password";color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Too simple password"; color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "The average password";color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Good password";color="green"}
                getElementById('bad_pass1').innerHTML = text; // Выводим итоговую сложность пароля
                //return false;
                break
            }
            case 'Отправить':
            {
                if (password.length < 6 && rating < 3){ text = "Слишком простой пароль";color="red"}
                else if (password.length < 6 && rating >= 3) {text = "Средний пароль";color="yellow"}
                else if (password.length >= 8 && rating < 3){text = "Средний пароль";color="yellow"}
                else if (password.length >= 8 && rating >= 3) {text = "Хороший пароль";color="green"}
                else if (password.length >= 6 && rating == 1) {text = "Слишком простой пароль";color="red"}
                else if (password.length >= 6 && rating > 1 && rating < 4) {text = "Средний пароль";color="yellow"}
                else if (password.length >= 6 && rating == 4) {text = "Хороший пароль";color="green"}
                getElementById('bad_pass1').innerHTML = text; // Выводим итоговую сложность пароля
                //return false;
                break
            }
            default:{
                getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                    'Text not found!' : '';
                break
            }
        }

        getElementById('bad_pass1').style.color = color;
        // with (document) {
        //  getElementById('bad_pass1').innerHTML = (getElementById('pass1').value.length < 5 ) ?
        //          'Пароль закороткий!' : '';
        //  getElementById('bad_pass2').innerHTML='repeat';

    }
}

function checkPass2 ()
{
    with (document)     {
        var firstRow = getElementById("translate").innerHTML;
        switch  (firstRow){
        case 'Заповніть усі поля:':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Пароль повторений невірно!' : '';
            break
        }
        case 'Fill in all fields:':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Password incorrectly repeated!' : '';
            break
        }
        case 'Заполните все поля:':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Пароль повторен неверно!' : '';
            break
        }
        case 'Надіслати':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Пароль повторений невірно!' : '';
            break
        }
        case 'Submit':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Password incorrectly repeated!' : '';
            break
        }
        case 'Отправить':
        {
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Пароль повторен неверно!' : '';
            break
        }
        default:{
            getElementById ('bad_pass2').innerHTML = (getElementById ('passwordReg').value != getElementById ('repeatPassword').value) ?
                'Text not found!' : '';
            break
        }
    }}
}
